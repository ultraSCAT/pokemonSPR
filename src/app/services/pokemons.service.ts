import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, zip } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { PokemonData,PokemonShare } from '../models/pokemon.model';
import { PokemonType } from '../models/pokemon.type.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  public pokemonShareTemplate = { name: '', image: '', sprite: '', type: '' };
  private types: Array<string> = ['water', 'fire', 'grass'];
  constructor(private http: HttpClient) {}

  getRandomType() {
    return this.types[Math.floor(Math.random() * this.types.length)];
  }

  selectRandomPokemon(coleccionPokemons: PokemonType) {
    const arrayLength = coleccionPokemons.pokemon.length;
    const randomIndex = Math.floor(Math.random() * arrayLength);
    const randomName = coleccionPokemons.pokemon[randomIndex].pokemon.url;
    return randomName;
  }

  // Se concatena la request para buscar pokemons por tipo con la busqueda del pokemon elegido
  getPokemonByTypee(pokeType: string): Observable<PokemonShare> {
    return this.http
      .get<PokemonType>(environment.pokeUrlBase + 'type/' + pokeType, {
        headers: new HttpHeaders().set('Accept', 'applicaion/json'),
      })
      .pipe(
        mergeMap((res) =>
          zip(
            of(res),
            this.http.get<PokemonData>(this.selectRandomPokemon(res))
          )
        ),
        map(([tipo, pokemon]) => ({
          type: tipo.name,
          name: pokemon.name,
          sprite: pokemon.sprites.front_default,
          image: pokemon.sprites.other['official-artwork'].front_default,
        }))
      );
  }
}
