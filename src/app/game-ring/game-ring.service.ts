import { Injectable, EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PokemonShare } from '../models/pokemon.model';
import {  filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GameRingService {
  public newType$: EventEmitter<string> = new EventEmitter<string>();
  public pokemonShareInit: PokemonShare = {
    name: '',
    image: '',
    sprite: '',
    type: '',
  };
  private weapons = {
    fire: { weakTo: 'water', strongTo: 'grass' },
    water: { weakTo: 'grass', strongTo: 'fire' },
    grass: { weakTo: 'fire', strongTo: 'water' },
  };
  private contador = 0; //pokebals volteadas
  private finCiclo: Subject<number> = new Subject<number>();
  private pokebalsState$: Subject<boolean> = new Subject<boolean>();
  private typeSelected$: Subject<string> = new Subject<string>();
  constructor() {}

  get ciclo(): Observable<number>{
    return this.finCiclo.pipe(filter((x) => x === x));
  }

  getPokebals(): Observable<boolean> {
    return this.pokebalsState$.asObservable();
  }

  setPokebalState(orden: boolean) {
    this.pokebalsState$.next(orden);
  }

  getTypeSelected(): Observable<string> {
    return this.typeSelected$.asObservable();
  }

  setTypeSelected(type: string) {
    this.newType$.emit(type);
    this.typeSelected$.next(type);
  }

  compareWinner(tipoPokemonJugador: string, tipoPokemonRival: string): any {
    if (this.weapons[tipoPokemonJugador].strongTo === tipoPokemonRival) {
      return { mensaje: 'gana', emoji: '⭕', punto: 1 };
    }
    if (this.weapons[tipoPokemonJugador].weakTo === tipoPokemonRival) {
      return { mensaje: 'pierde', emoji: '❌', punto : -1 };
    }
    return { mensaje: 'empate', emoji: '=', punto: 0 };
  }

  setCiclo(volteado: number) {
    this.contador += volteado;
    this.finCiclo.next(this.contador);
  }

  restartCount() {
    this.contador = 0;
  }

}
