import {
  Component,
  Input,
  Output,
  ViewEncapsulation,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChild,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { PokemonShare } from '../../models/pokemon.model';
import { PokemonService } from '../../services/pokemons.service';
import SwiperCore, { EffectFlip, Swiper, Virtual } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
import { SoundsService } from 'src/app/services/sounds.service';
import { GameRingService } from 'src/app/game-ring/game-ring.service';
import { Observable, Subscription } from 'rxjs';

// install Swiper modules
SwiperCore.use([EffectFlip]);
SwiperCore.use([Virtual]);
@Component({
  selector: 'app-pokeball',
  templateUrl: './pokeball.component.html',
  styleUrls: ['./pokeball.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PokeballComponent implements OnChanges,OnInit, OnDestroy {
  @Input() typeSelected$: Observable<string> = new Observable<string>();
  @Input() pokemonReceived = this.gameRingService.pokemonShareInit;
  @Output() pokemonDatos = new EventEmitter<PokemonShare>(true);
  @ViewChild('swipeBigPokeball', { static: false }) swiper?: SwiperComponent;

  private pokemonData: PokemonShare = this.gameRingService.pokemonShareInit;
  private pokeType: any;
  private subscriptions = new Subscription();
  constructor(
    private pokemonService: PokemonService,
    private soundsService: SoundsService,
    private gameRingService: GameRingService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.pokemonReceived);
    if(this.pokemonReceived.type !== '') {
      this.pokemonData = this.pokemonReceived;
      this.startPokeballCycle();
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(
      this.gameRingService.getPokebals().subscribe({
      next: (hideOrder) => {
        console.log('Esconder');
        this.voltear();}
    }));

    this.subscriptions.add(this.typeSelected$.subscribe({
      next: (typeIputed) => {
        this.pokeType = typeIputed;
        if (this.pokeType === 'random') {
          this.pokeType = this.pokemonService.getRandomType();
        }
        if (this.pokeType) {
          this.getPokemonByType(this.pokeType);
        }
      },
    }));
  }

  delay = (ms) => new Promise((res) => setTimeout(res, ms));

  emitirDatosPokemon() {
    this.pokemonDatos.emit(this.pokemonData);
  }

  protected getPokemonByType(type: string) {
    this.subscriptions.add(this.pokemonService.getPokemonByTypee(type).subscribe({
      next: (res) => {
        if (!this.resourcesOk(res)) {
          this.getPokemonByType(this.pokeType);
          return false;
        }
        console.log(res);
        this.pokemonData = res;
        this.startPokeballCycle();
      },
    }));
  }
  protected startPokeballCycle() {
    this.slideNext();
    this.emitirDatosPokemon();
    this.soundsService.playWooshSound();
    this.gameRingService.setCiclo(1);
  }

  private resourcesOk(pokemonRes) {
    return pokemonRes.sprite && pokemonRes.image;
  }

  private voltear() {
      this.slidePrev();
      this.delay(200).then(() => {
        this.resetValues();
        this.gameRingService.restartCount();
      });
  }

  private slideNext() {
    this.swiper.swiperRef.slideNext(500);
  }

  private slidePrev() {
    this.swiper.swiperRef.slidePrev(200);
  }

  private resetValues() {
    this.pokemonData = this.gameRingService.pokemonShareInit;
    this.pokemonDatos.emit(this.pokemonData);
  }
}
