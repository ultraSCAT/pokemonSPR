import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { interval, Observable, Subject, Subscription } from 'rxjs';
import { scan, takeWhile, startWith, switchMap, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-temporizador',
  templateUrl: './temporizador.component.html',
  styleUrls: ['./temporizador.component.scss'],
})
export class TemporizadorComponent implements OnInit {
  @Output() progressCompleted = new EventEmitter<void>();
  restart$ = new Subject();
  progress = 0;
  color = 'success';
  private progress$: Observable<number>;
  private sub: Subscription;

  constructor(){
    this.progress$ = this.restart$.pipe(
      startWith(0),
      switchMap(() => interval(300).pipe(
        scan((acc, val) => acc + 0.1, 0),
        takeWhile(val => val < 1)
      ))
    );
  }

  onComplete(){
      this.progressCompleted.emit();
  }

  ngOnInit() {
    this.sub = this.progress$.subscribe(val => {
      this.progress = val;
      if (val >= 0.99999) {
        this.onComplete();
      }
      if (val >= 0.7) {
        return this.color = 'danger';
      }
      if (val >= 0.4) {
        return this.color = 'warning';
      }
    });
  }

  stopCount(){
    this.sub.unsubscribe();
  }

  handleRestart(){
    this.progress = 0;
    this.color = 'success';
    this.restart$.next();
  }
}
