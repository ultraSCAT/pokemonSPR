import { NgModule, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SwiperModule } from 'swiper/angular';
import { CountdownModule } from 'ngx-countdown';


import { GameRingPageRoutingModule } from './game-ring-routing.module';
import { GameRingPage } from './game-ring.page';
import { PokeballComponent } from '../components/pokeball/pokeball.component';
import { PokeballModule } from '../components/pokeball/pokeball.module';
import { TemporizadorComponent } from '../components/temporizador/temporizador.component';

@NgModule({
  imports: [
    CommonModule,FormsModule,
    IonicModule,GameRingPageRoutingModule,
    SwiperModule,PokeballModule,CountdownModule
  ],
  providers:[
    Pipe
  ],
  declarations: [
    GameRingPage,PokeballComponent,
    TemporizadorComponent,
  ]
})

export class GameRingPageModule {

  constructor(){

  }
}
