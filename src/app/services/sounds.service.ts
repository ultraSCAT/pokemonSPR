import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SoundsService {
  private stream: HTMLAudioElement = new Audio();
  private musica: HTMLAudioElement = new Audio();
  private shared: HTMLAudioElement = new Audio();
  private efectos = {
    gana: {
      src : 'assets/audio/sounds/correct.mp3',
      volume : 0.9
    },
    pierde: {
      src: 'assets/audio/sounds/wrong.mp3',
      volume: 0.2
    },
    empate: {
      src: 'assets/audio/sounds/hit.mp3',
      volume: 0.2
    },
    musica: {
      src: 'assets/audio/background/pokemonMusica.mp3',
      volume: 0.3
    }
  };
  constructor() { }

  playWooshSound(){
    this.shared.muted = false;
    this.shared.volume = 0.4;
    this.shared.src = 'assets/audio/sounds/whoosh.mp3';
    this.shared.play();
  }

  playMusicBackground(src){
    this.musica.volume = this.efectos[src].volume;
    this.musica.src = this.efectos[src].src;
    this.musica.muted = false;
    this.musica.play();
  }

  toggleMusic(){
    if(this.musica.paused){
      return this.musica.play();
    }
    this.musica.pause();
  }

  stopSound(src){
    this.stream.volume = 0.2;
    this.stream.src = src;
    this.stream.pause();
  }

  playSound(tipo){
    this.stream.volume = this.efectos[tipo].volume;
    this.stream.src = this.efectos[tipo].src;
    this.stream.play();
  }

  playPikachuSound(){
    this.stream.muted = false;
    this.stream.volume = 0.8;
    this.stream.src = 'assets/audio/sounds/pikachu.mp3';
    this.stream.play();
  }

}
