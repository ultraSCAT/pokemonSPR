import { TestBed } from '@angular/core/testing';

import { GameRingService } from './game-ring.service';

describe('GameRingService', () => {
  let service: GameRingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameRingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
