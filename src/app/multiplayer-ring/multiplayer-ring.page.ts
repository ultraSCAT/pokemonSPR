import { AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';


import { SoundsService } from '../services/sounds.service';
import { GameRingService } from '../game-ring/game-ring.service';
import { PokemonShare } from '../models/pokemon.model';
import io from 'socket.io-client';

@Component({
  selector: 'app-multiplayer-ring',
  templateUrl: './multiplayer-ring.page.html',
  styleUrls: ['./multiplayer-ring.page.scss'],
})
export class MultiplayerRingPage implements OnInit, OnDestroy, AfterViewInit{
  public puntaje = 0;
  public tipoSeleccionado?: string;
  public tipoSeleccionado$: Subject<string> = new Subject<string>();
  public pokemonRival: PokemonShare = this.gameRingService.pokemonShareInit;
  public pokemonJugador: PokemonShare = this.gameRingService.pokemonShareInit;
  public enemyName = 'Enemigo';
  private socket: any;
  private subscriptions = new Subscription();

  constructor(
    private soundsService: SoundsService,
    private gameRingService: GameRingService,
    private router: Router,
  ) {}

  emitPokemon(pokemonRequested){
    this.pokemonJugador = pokemonRequested;
    //only emmits when pokemon has values, sometimes is is just empty to reset values
    if(pokemonRequested.type) {this.socket.emit('playerSelects',this.pokemonJugador);}
  }

  delay = (ms) => new Promise((res) => setTimeout(res, ms));

  ngOnInit() {
    this.socket = io('https://pokemonsocket.onrender.com');
    this.socket.on('showEnemy', pokemonEnemy => {
      this.iniciarCiclo(pokemonEnemy);
    });

    this.socket.on('enemyDown', () => {
      this.reloadWindow();
    });
  }

  ngAfterViewInit(): void {
    this.soundsService.playMusicBackground('musica');
    this.soundsService.playPikachuSound();

    // espera a que las dos pokeballs sean traidas
    // toma un evento de las dos pokeballs
    this.subscriptions.add(
      this.gameRingService.ciclo.subscribe({
      next: (showedPokemons) => {
        const comparadorHTML = document.querySelector('.comparador');
        // valida que el contador de pokemons volteados sea 2
        if (showedPokemons !== 2) {
          console.log('Llega');
          return false;
        }
        this.delay(1000).then(() => {
          const ganadorRes = this.gameRingService.compareWinner(
            this.tipoSeleccionado,
            this.pokemonRival.type
          );
          comparadorHTML.textContent = ganadorRes.emoji;
          this.soundsService.playSound(ganadorRes.mensaje);
          this.delay(1000).then(() => {
            this.puntaje += ganadorRes.punto;
            this.esperarParaIniciar();
          });
        });
      }})
    );
  }

  ngOnDestroy() {
    this.socket.disconnect();
    this.soundsService.toggleMusic();
    this.subscriptions.unsubscribe();
    this.gameRingService.restartCount();
  }

  iniciarCiclo(tipo) {
    this.delay(2000).then(() => {
      this.voltearEnemigo(tipo);
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.router.navigate(['multijugdor']);
      event.target.complete();
    }, 100);
  }

  tipoSelected(tipoClicado: string) {
    this.toggleOpciones(false);
    this.tipoSeleccionado$.next(tipoClicado);
    this.tipoSeleccionado = tipoClicado;
  }

  voltearEnemigo(pokemonReceived: PokemonShare) {
    this.pokemonRival = pokemonReceived;
  }

  toggleOpciones(orden: boolean) {
    const lista = document.querySelector('#footerEleccion');
    if (orden) {
      return lista.classList.remove('no-click');
    }
    return lista.classList.add('no-click');
  }

  esperarParaIniciar(){
    this.delay(1200).then(() => {
      this.toggleOpciones(true);
      this.gameRingService.setPokebalState(true);
      document.querySelector('.comparador').textContent = 'VS';
      this.socket.emit('approveSelections');
    });
  }

  toggleMusic() {
    this.soundsService.toggleMusic();
  }

  reloadWindow(){
    location.reload();
  }
}
