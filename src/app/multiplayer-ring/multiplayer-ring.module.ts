import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MultiplayerRingPageRoutingModule } from './multiplayer-ring-routing.module';

import { MultiplayerRingPage } from './multiplayer-ring.page';
import { PokeballComponent } from '../components/pokeball/pokeball.component';
import { SwiperModule } from 'swiper/angular';
import { PokeballModule } from '../components/pokeball/pokeball.module';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
  imports: [
    CommonModule,FormsModule,
    IonicModule,MultiplayerRingPageRoutingModule,
    SwiperModule,PokeballModule,CountdownModule
  ],
  declarations: [MultiplayerRingPage,PokeballComponent]
})
export class MultiplayerRingPageModule {}
