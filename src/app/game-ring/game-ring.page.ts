import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

// import Swiper core and required modules
import SwiperCore, { Virtual } from 'swiper';
import { AlertController } from '@ionic/angular';

import { SoundsService } from '../services/sounds.service';
import { GameRingService } from './game-ring.service';
import { PokemonShare } from '../models/pokemon.model';
import { TemporizadorComponent } from '../components/temporizador/temporizador.component';
import io from 'socket.io-client';

// install Swiper modules
SwiperCore.use([Virtual]);

@Component({
  selector: 'app-game-ring',
  templateUrl: './game-ring.page.html',
  styleUrls: ['./game-ring.page.scss'],
})
export class GameRingPage implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(TemporizadorComponent) temporizador: TemporizadorComponent;
  public puntaje = 0;
  public tipoSeleccionado?: string;
  public tipoEnemigo$: Subject<string> = new Subject<string>();
  public tipoSeleccionado$: Subject<string> = new Subject<string>();
  public pokemonRival: PokemonShare = this.gameRingService.pokemonShareInit;
  public pokemonJugador: PokemonShare = this.gameRingService.pokemonShareInit;
  private socket: any;

  constructor(
    private soundsService: SoundsService,
    private gameRingService: GameRingService,
    private router: Router,
    private alertController: AlertController
  ) {}

  delay = (ms) => new Promise((res) => setTimeout(res, ms));

  ngOnInit() {
    this.iniciarCiclo();
  }

  ngAfterViewInit(): void {
    this.soundsService.playMusicBackground('musica');
    this.soundsService.playPikachuSound();
  }

  ngOnDestroy() {
    this.soundsService.toggleMusic();
  }

  iniciarCiclo() {
    this.delay(2000).then(() => {
      this.voltearEnemigo();
      this.delay(900).then(() => {
        this.toggleOpciones(true);
      });
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      // this.ngOnInit();
      this.router.navigate(['batalla']);
      event.target.complete();
    }, 100);
  }

  tipoSelected(tipoClicado: string) {
    const comparadorHTML = document.querySelector('.comparador');
    this.temporizador.stopCount();
    this.tipoSeleccionado$.next(tipoClicado);
    this.toggleOpciones(false);
    this.tipoSeleccionado = tipoClicado;

    // espera a que las dos pokeballs sean traidas
    // toma un evento de las dos pokeballs
    this.gameRingService.ciclo.pipe(take(1)).subscribe({
      next: (showedPokemons) => {
        // valida que el contador de pokemons volteados sea 2
        if (showedPokemons !== 2) {
          return false;
        }
        this.delay(1000).then(() => {
          const ganadorRes = this.gameRingService.compareWinner(
            this.tipoSeleccionado,
            this.pokemonRival.type
          );
          comparadorHTML.textContent = ganadorRes.emoji;
          this.soundsService.playSound(ganadorRes.mensaje);
          if(ganadorRes.punto < 0){
            return this.gameEnded();
          }
          this.delay(1000).then(() => {
            this.puntaje += ganadorRes.punto;
            this.esperarParaIniciar();
          });
        });
      },
    });
  }

  voltearEnemigo() {
    this.tipoEnemigo$.next('random');
  }

  toggleOpciones(orden: boolean) {
    const lista = document.querySelector('#footerEleccion');
    if (orden) {
      return lista.classList.remove('no-click');
    }
    return lista.classList.add('no-click');
  }

  esperarParaIniciar(){
    this.delay(1200).then(() => {
      this.gameRingService.setPokebalState(true);
      document.querySelector('.comparador').textContent = 'VS';
      this.iniciarCiclo();
    });
  }

  toggleMusic() {
    this.soundsService.toggleMusic();
  }

  async gameEnded() {
    const alert = await this.alertController.create({
      header: '',
      subHeader: `Lograste ${this.puntaje} aciertos`,
      message: 'Parece que alguien debe repasar la Pokedex!',
      buttons: ['OK'],
    });
    alert.onDidDismiss().then(() => {
      // this.reloadWindow();
      this.puntaje = 0;
      this.esperarParaIniciar();
    });
    await alert.present();
  }

  reloadWindow(){
    location.reload();
  }
}
