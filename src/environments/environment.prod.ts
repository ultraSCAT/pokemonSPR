export const environment = {
  production: true,
  pokeUrlBase: 'https://pokeapi.co/api/v2/',
  bgMusicSrc: 'assets/audio/background/pokemonMusica.mp3'
};
