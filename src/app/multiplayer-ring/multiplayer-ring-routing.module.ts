import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MultiplayerRingPage } from './multiplayer-ring.page';

const routes: Routes = [
  {
    path: '',
    component: MultiplayerRingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MultiplayerRingPageRoutingModule {}
